/**
 * @description       : 
 * @author            : ethan.k@digitalmass.com
 * @group             : 
 * @last modified on  : 07-29-2020
 * @last modified by  : ethan.k@digitalmass.com
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   07-29-2020   ethan.k@digitalmass.com   Initial Version
**/
// imports
export default class BoatTile extends LightningElement {
    boat;
    selectedBoatId;
    
    // Getter for dynamically setting the background image for the picture
    get backgroundStyle() { }
    
    // Getter for dynamically setting the tile class based on whether the
    // current boat is selected
    get tileClass() { }
    
    // Fires event with the Id of the boat that has been selected.
    selectBoat() { }
  }
  