/**
 * @description       : 
 * @author            : ethan.k@digitalmass.com
 * @group             : 
 * @last modified on  : 07-29-2020
 * @last modified by  : ethan.k@digitalmass.com
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   07-29-2020   ethan.k@digitalmass.com   Initial Version
**/
 // imports
 import { LightningElement } from 'lwc';
 export default class BoatSearch extends LightningElement {
    isLoading = false;
    
    // Handles loading event
    handleLoading() { }
    
    // Handles done loading event
    handleDoneLoading() { }
    
    // Handles search boat event
    // This custom event comes from the form
    searchBoats(event) { }
    
    createNewBoat() { }
  }
  