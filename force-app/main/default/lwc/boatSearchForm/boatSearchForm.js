/**
 * @description       : 
 * @author            : ethan.k@digitalmass.com
 * @group             : 
 * @last modified on  : 07-29-2020
 * @last modified by  : ethan.k@digitalmass.com
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   07-29-2020   ethan.k@digitalmass.com   Initial Version
**/
// import getBoatTypes from the BoatDataService => getBoatTypes method';
import {LightningElement, api, track, wire } from 'lwc';
//import getBoats from '@salesforce/apex/BoatDataService.getBoats';
import getBoatTypes from '@salesforce/apex/BoatDataService.getBoatTypes';

export default class BoatSearchForm extends LightningElement {
    @track selectedBoatTypeId = '';
    @track searchOptions;
    // Private
    error = undefined;
    
    // Needs explicit track due to nested data
    @track searchOptions;
    
    // Wire a custom Apex method
      @wire(getBoatTypes)
      boatTypes({ error, data }) {
      if (data) {
        this.searchOptions = data.map(type => {
           return {
                label : type.Name,
                value : type.Id
           } 
        });
        this.searchOptions.unshift({ label: 'All Types', value: '' });
        } else if (error) {
                this.searchOptions = undefined;
                this.error = error;
            }
        }
    
    // Fires event that the search option has changed.
    // passes boatTypeId (value of this.selectedBoatTypeId) in the detail
    handleSearchOptionChange(event) {
      // Create the const searchEvent
      this.selectedBoatTypeId = event.detail.value;
      // searchEvent must be the new custom event search
      const searchEvent = new CustomEvent('search', 
        {detail: {
            boatTypeId: this.selectedBoatTypeId}
        });
      this.dispatchEvent(searchEvent);
    }
  }
  